import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {numericSort, filterById} from './../utils/utils';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnChanges {

  @Input()
  values: Array<any>;
  @Input()
  horizons: Array<any>;
  filtered: Array<any>;

  @Output()
  deleteEmitter: EventEmitter<any>;

  orderSelect = new FormControl('1');
  filterSelect = new FormControl('0');


  constructor() {
    this.deleteEmitter = new EventEmitter<any>();
  }

  ngOnChanges(changes: SimpleChanges) {
    const chng = changes['values'];
    this.values = chng.currentValue;
    this.filtered = this.values;
    this.filtered = this.filter();
    this.filtered = this.sort();
  }

  ngOnInit() {
    this.filtered = this.values;
    // this.filtered = this.filter();
    // this.filtered = this.sort();
  }

  onOrder() {
    this.filtered = this.sort();
  }

  onFilter() {
    this.filtered = this.filter();
    this.filtered = this.sort();
  }

  onDelete(id) {
    this.deleteEmitter.emit(id);
  }

  sort() {
    const compareFieldName = 'rent';
    const direction = parseInt(this.orderSelect.value, 10);
    return numericSort(this.filtered, compareFieldName, direction);
  }

  filter() {
    const value = parseInt(this.filterSelect.value, 10);
    return filterById(this.values, value, 'horizon');
  }

}

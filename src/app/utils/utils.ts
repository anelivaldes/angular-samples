export function numericSort(items, field, direction) {
  let elements = [...items];
  elements = elements.sort((a, b) => {
    return direction > 0 ? a[field] - b[field] : b[field] - a[field];
  });
  return elements;
}

export function filterById(items, value, field) {
  let elements = [...items];
  if (value !== 0) {
    elements = elements.filter((v) => v[field].id === value);
  }
  return elements;
}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListContainerComponent} from './list-container/list-container.component';


const routes: Routes = [
  {
    path: 'list',
    component: ListContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

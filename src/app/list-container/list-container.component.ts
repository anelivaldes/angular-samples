import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-list-container',
  templateUrl: './list-container.component.html',
  styleUrls: ['./list-container.component.scss']
})
export class ListContainerComponent implements OnInit {
  values = [
    {id: 0, rent: 1.45, horizon: {id: 2, label: 'Más de 1 año'}},
    {id: 1, rent: 3.25, horizon: {id: 3, label: 'Más de 2 años'}},
    {id: 2, rent: 12.2, horizon: {id: 1, label: 'Menos de 1 año'}},
    {id: 3, rent: 0.2, horizon: {id: 1, label: 'Menos de 1 año'}}
  ];
  horizons = [
    {id: 0, label: 'Todos'},
    {id: 1, label: 'Menos de 1 año'},
    {id: 2, label: 'Más de 1 año'},
    {id: 3, label: 'Más de 2 años'},
  ];
  horizonsValues;
  horizonsNames = {
    1: 'Menos de 1 año',
    2: 'Más de 1 año',
    3: 'Más de 2 años',
  };

  horizonsSelect = new FormControl('1');
  rentInput = new FormControl(1.0, Validators.required);

  constructor() {
  }

  ngOnInit() {
    this.horizonsValues = this.horizons.filter((h) => h.id !== 0);
  }

  onAdd() {
    const id = parseInt(this.horizonsSelect.value, 10);
    this.values = [...this.values, {
      id: this.values.length,
      rent: this.rentInput.value, horizon: {
        id,
        label: this.horizonsNames[id]
      }
    }];
  }


  onDeleteItem($event) {
    this.values = [...this.values.filter(v => v.id !== $event)];
  }
}
